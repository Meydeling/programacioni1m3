/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import java.util.Arrays;
import java.util.Comparator;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author DocenteFCyS
 */
public class EmpleadoData {
    private Empleado[] empleados;
    private MunicipioData mData;

    public EmpleadoData() {
        mData = new MunicipioData();        
    }
    
    public void add(Empleado e){
        empleados = addEmpleado(empleados, e);
    }
    
    private Empleado[] addEmpleado(Empleado[] eCopy, Empleado e){
        if(eCopy == null){
            eCopy = new Empleado[1];
            eCopy[eCopy.length - 1] = e;
            return eCopy;
        }
        
        eCopy = Arrays.copyOf(eCopy,eCopy.length + 1);
        eCopy[eCopy.length - 1] = e;
        
        return eCopy;
    }
    
    public Empleado[] getEmpleados(){
        return this.empleados;
    }
    
    public Empleado getEmpledoByCodigo(int codigo){
        if(codigo <= 0){
            return null;
        }
            
        Arrays.sort(empleados);
        int index = Arrays.binarySearch(empleados, new Empleado(codigo));
        if(index < 0){
            return null;
        }
        
        return empleados[index];
    }
    
    public Empleado getEmpleadoByCedula(String cedula){
        if(cedula == null){
            return null;
        }
        //el metodo isBlank necesita el jdk 11 o superior
        if(cedula.isBlank()){
            return null;
        }
        Empleado e = new Empleado();
        e.setCedula(cedula);
        EmpleadoCedulaComparator ecc = new EmpleadoCedulaComparator();
        Arrays.sort(empleados, ecc);
        int index = Arrays.binarySearch(empleados, e, ecc);
        
        if(index < 0){
            return null;
        }
        
        return empleados[index];
    }
    
    
}
