/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleDemo {
    public static void start() throws IOException{
        int opc = 0;
        boolean flag = true;
        BufferedReader reader = 
                new BufferedReader(
                        new InputStreamReader(System.in));
        do{
            ConsoleMenus.mainMenu();
            System.out.print("Opcion: ");
            opc = Integer.parseInt(reader.readLine());
            
            switch(opc){
                case 1:
                        gestionEmpleado(reader);
                        break;
                case 2:
                        reportesEmpleado();
                        break;
                case 3:
                        System.out.println("Hasta luego!");
                        flag = false;
                        break;
                default:
                        System.err.println("La opcion seleccionada no es valida!");
            }
        }while(flag);
    }
    
    public static void gestionEmpleado(BufferedReader reader) throws IOException{
        int opc = 0;
        boolean flag = true;
        do{
            ConsoleMenus.gestionSubMenu();
            opc = ConsoleReader.readInt(reader, "Opcion: ");
            
            switch(opc){
                case 1:
                case 2:
                case 3:
                case 4:
                default:
            }
        }while(flag);
        
        /*TODO
            MOSTRAR EL SUBMENU DE GESTION
            Y GARANTIZAR LOS METODOS PARA AGREGAR, EDITAR
            Y ELIMINAR UN EMPLEADO
        */
    }
    
    public static void reportesEmpleado(){
        /*TODO
            CREAR LOS METODOS QUE NOS PERMITA 
            FILTRAR LOS EMPLEADOS POR LAS
        DIFERENTES VISUALIZACIONES
        */
    }
}
