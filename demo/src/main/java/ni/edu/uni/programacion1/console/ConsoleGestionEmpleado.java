/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import ni.edu.uni.programacion1.enums.NivelAcademico;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleGestionEmpleado {
    public static Empleado readEmpleado(BufferedReader reader) throws IOException{
        System.out.println("A continuacion se le va solicitar informacion del empleado");
        int codigo = ConsoleReader.readInt(reader, "Codigo: ");
        String cedula = ConsoleReader.readCedula(reader, "Cedula: ");
        
        
        
        return new Empleado(codigo, cedula, cedula, cedula, cedula, cedula, cedula, cedula, cedula, Sexo.FEMENINO, NivelAcademico.LICENCIADO);
    }
}
