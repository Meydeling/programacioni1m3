/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import ni.edu.uni.programacion1.pojo.Municipio;

/**
 *
 * @author DocenteFCyS
 */
public class MunicipioData {
    private Municipio[] municipios;
    private DepartamentoData departamentodata;

    public MunicipioData() {
        departamentodata = new DepartamentoData();
        populateMunicipios();
    }
    
    private void populateMunicipios(){
        municipios = new Municipio[]{
            new Municipio(1, "Boaco", 
                    departamentodata.getDepartamentoById(1)),
            new Municipio(2, "Camoapa", 
                    departamentodata.getDepartamentoById(1)),
            new Municipio(3, "San Lorenzo", 
                    departamentodata.getDepartamentoById(1)),
            new Municipio(4, "San Jose de los Remates", 
                    departamentodata.getDepartamentoById(1)),
            new Municipio(5, "Santa Lucia", 
                    departamentodata.getDepartamentoById(1)),
            new Municipio(6, "Teustepe", 
                    departamentodata.getDepartamentoById(1))
        };
    }
    
}
