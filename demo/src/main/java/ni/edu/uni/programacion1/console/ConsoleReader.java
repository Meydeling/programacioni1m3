/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ni.edu.uni.programacion1.util.Validator;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleReader {
    public static int readInt(BufferedReader reader, String message) throws IOException{
        boolean flag = true;
        String text = "";
        do{
            System.out.print(message);
            text = reader.readLine();
            flag = Validator.validateIsInteger(text);
        }while(!flag);
        return Integer.parseInt(text);
    }    
    
    public static String readLetter(BufferedReader reader, String message) throws IOException{
        boolean flag = true;
        String text = "";
        do{
            System.out.print(message);
            text = reader.readLine();
            flag = Validator.validateIsLetters(text);
        }while(!flag);
        return text;
    }        
    
    public static String readCedula(BufferedReader reader, String message) throws IOException{
        boolean flag = true;
        String text = "";
        do{
            System.out.print(message);
            text = reader.readLine();
            flag = Validator.validateIsCedula(text);
        }while(!flag);
        return text;
    }
   
            
}
