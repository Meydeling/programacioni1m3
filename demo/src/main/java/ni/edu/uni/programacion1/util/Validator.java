/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author DocenteFCyS
 */
public class Validator {

    public static boolean validateIsInteger(String text) {        
        return validate("^\\d+$",text);
    }
    
    public static boolean validateIsLetters(String text){        
        return validate("/^[a-z ,.'-]+$/i",text);
    }
    
    public static boolean validateIsCedula(String text){
        return validate("",text);
    }
    
    public static boolean validateIsPhoneNumber(String text){
        return validate("",text);
    }
    
    private static boolean validate(String expression, String text){
        Pattern p = Pattern.compile(expression);
        Matcher m = p.matcher(text);
        return m.matches();
    }
}
